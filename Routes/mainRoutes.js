const express = require("express");

const candidatModel = require('../Models/CandidatModel');
const EmployerModel = require('../Models/EmployerModel');
const OffreModels = require('../Models/OffreModels');
const Validations = require('../Models/ValidationModel');
const ReseauxModel = require('../Models/ReseauxModel');
const CandidatureModel = require('../Models/CandidatureModel');
const TransactionModel = require('../Models/Transcations');

candidatModel.hasMany(CandidatureModel);
CandidatureModel.belongsTo(candidatModel);
CandidatureModel.belongsTo(OffreModels);
OffreModels.hasMany(CandidatureModel);

CandidatureModel.hasMany(Validations);
Validations.belongsTo(CandidatureModel);

EmployerModel.hasMany(Validations);
Validations.belongsTo(EmployerModel);

const router = express.Router();

router.get("/", (req, res) => {
    res.send(JSON.stringify({response: "goood"}))
});

module.exports = router;

