const express = require("express");

const employeeModel = require('../Models/EmployerModel');
const {returnCustom, returnError, returnSuccess} = require("../Utils/respnseUtils");

const router = express.Router();

const Blochaine = require("../BlockChain");

const bchain = new Blochaine();
//defining routes

router.get("/login", (req, res) => {
    const  email = req.query.email;
    const  password = req.query.password;
    console.log(req);

    employeeModel.findOne({
        where: {
            email: email,
            password: password
        }
    }).then(employee => {
        bchain.addBlock(JSON.stringify(employee));
        if (employee !== null){
            res.send(returnCustom("Login correct", employee))
        }else {
            res.send(returnError("Login ou mots de passe incorrect"))
        }
    })

});

router.get("/getAll", (req, res) => {

    employeeModel.findAll(
        {
            attributes:['id_employer','nom_complet', 'email', 'fonction', 'password']
        }
        ).then(candidat => {
        if (candidat !== null){
            bchain.addBlock(JSON.stringify(candidat));
            res.send(candidat)
        }else {
            res.send(returnError("Login ou mots de passe incorrect"))
        }
    })

});

router.post("/create", (req, res) => {
    const employeedata = req.body.employee;
    console.log(req);
    employeeModel.create({
        nom_complet : employeedata.nom_complet,
        email: employeedata.email,
        password: '123456',
        fonction: employeedata.fonction
    }).then(candidat => {
        bchain.addBlock(JSON.stringify(candidat));
        res.send(returnCustom("Vous vous etes enregistrer avec succes", candidat))})
        .catch(err => res.send(returnError("Erreur lors de la creation d'un compte")))
});
router.get("/delete", (req, res) => {
    const employeedata = req.query.id_employee;
    console.log(req);
    employeeModel.findOne({
        where: {
            id_employer: employeedata
        }
    }).then(candidat => {
        bchain.addBlock(JSON.stringify(candidat));
        candidat.destroy().then(cand => res.send(returnCustom("Employee supprimee", cand)))
    })
        .catch(err => {
            console.log(err)
            res.send(returnError("Erreur lors de la creation d'un compte"))
        })
});


module.exports = router;
