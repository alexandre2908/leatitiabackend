const express = require("express");
const candidatModel = require('../Models/CandidatModel');
const {returnCustom, returnError, returnSuccess} = require("../Utils/respnseUtils");
const router = express.Router();

const offreData = require("../Models/OffreModels");
const Blochaine = require("../BlockChain");

const bchain = new Blochaine();

router.get("/getAll", (req, res) => {
    offreData.findAll({
        attributes: ['id_offre', 'titre', 'description','date_fin']
    })
        .then(offre => {
            bchain.addBlock(JSON.stringify(offre));
            if (offre !== null){
                res.send(offre)
            }else {
                res.send(returnError("Erreur lors de la recherche"))
            }
        })
        .catch(err => console.log(err))
});

router.get("/getOne", (req, res) => {
    offreid = req.query.id_offre;
    offreData.findOne(
        {
            attributes: ['id_offre', 'titre', 'description','date_fin'],
            where: {id_offre: offreid}
        }
        )
        .then(offretrouv => {
            bchain.addBlock(JSON.stringify(offretrouv));
            res.send(offretrouv)})
        .catch(err => console.log(err))
});

router.post("/create", (req, res) => {
    const offre = req.body.offres;
    offreData.create({
        titre : offre.titre,
        description: offre.description,
        date_fin: offre.date_fin
    }).then(offre => {
        bchain.addBlock(JSON.stringify(offre));
        res.send(returnSuccess("L'offre a été crée avec succée"))})
        .catch(err => res.send(returnError("Erreur lors de la creation de l'offre")))
});

router.get("/delete", (req, res) => {
    const offresid = req.query.id_offre;
    console.log(req);
    offreData.findOne({
        where: {
            id_offre: offresid
        }
    }).then(offre => {
        bchain.addBlock(JSON.stringify(offre));
        offre.destroy().then(off => res.send(returnCustom("Offre supprimee", off)))
    })
        .catch(err => {
            console.log(err)
            res.send(returnError("Erreur lors de la creation d'un compte"))
        })
});

module.exports = router;
