const express = require("express");

const adminModel = require('../Models/AdminModel');
const transactionModel = require('../Models/Transcations');
const {returnCustom, returnError, returnSuccess} = require("../Utils/respnseUtils");

const router = express.Router();
const Blochaine = require("../BlockChain");

const bchain = new Blochaine();

//defining routes

router.get("/login", (req, res) => {
    const  email = req.query.email;
    const  password = req.query.password;
    console.log(req);

    adminModel.findOne({
        where: {
            email: email,
            password: password
        }
    }).then(admin => {
        bchain.addBlock('{"email": email, password: password}').then(r =>{});
        if (admin !== null){
            res.send(returnCustom("Login correct", admin))
        }else {
            res.send(returnError("Login ou mots de passe incorrect"))
        }
    })

});

router.get("/transaction", (req,res) => {
    transactionModel.findAll().then(transaction => res.send(transaction))
});

module.exports = router;
