const express = require("express");
const multer = require('multer');
const candidatModel = require('../Models/CandidatModel');
const {returnCustom, returnError, returnSuccess} = require("../Utils/respnseUtils");

const router = express.Router();
const Blochaine = require("../BlockChain");

const bchain = new Blochaine();

//defining routes

router.get("/login", (req, res) => {
    const  email = req.query.email;
    const  password = req.query.password;
    console.log(req);

    candidatModel.findOne({
        where: {
            email: email,
            password: password
        }
    }).then(candidat => {
        bchain.addBlock(JSON.stringify(candidat));
        if (candidat !== null){
            res.send(returnCustom("Login correct", candidat))
        }else {
            res.send(returnError("Login ou mots de passe incorrect"))
        }
    })

});

router.post("/register", (req, res) => {
    const candidatdata = req.body.candidat;
    console.log(req);
    candidatModel.create({
        id_candidat: null,
        name : candidatdata.name,
        email: candidatdata.email,
        password: candidatdata.password,
        numero_telephone:candidatdata.numero_telephone
    }).then(candidat => {
        bchain.addBlock(JSON.stringify(candidat));
        res.send(returnCustom("Vous vous etes enregistrer avec succes", candidat))
    })
        .catch(err => res.send(returnError("Erreur lors de la creation d'un compte")))
});

module.exports = router;
