const express = require("express");
const multer = require('multer');
const CandidatureModel = require('../Models/CandidatureModel');
const candidatModel = require('../Models/CandidatModel');
const oreModel = require('../Models/OffreModels');
const validationModel = require('../Models/ValidationModel');
const employeeModel = require('../Models/EmployerModel');
const {returnCustom, returnError, returnSuccess} = require("../Utils/respnseUtils");
const path = require("path");

const Blochaine = require("../BlockChain");

const bchain = new Blochaine();

const router = express.Router();
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now()+ path.extname(file.originalname))
    }
});

var upload = multer({ storage: storage }).array('motivation[]', 2);

//defining routes

router.get("/getAll", (req, res) => {
    CandidatureModel.findAll({include: [candidatModel, oreModel]}).then(candidat => {
        if (candidat !== null){
            bchain.addBlock(JSON.stringify(candidat));
            res.send(candidat)
        }else {
            res.send(returnError("Erreur lors de la recheche des candidature"))
        }
    })
});
router.get("/getCandidat", (req, res) => {
    CandidatureModel.findAll({include: [candidatModel, oreModel], where: {candidatIdCandidat : req.query.id_candidat}}).then(candidat => {
        if (candidat !== null){
            bchain.addBlock(JSON.stringify(candidat));
            res.send(candidat)
        }else {
            res.send(returnError("Erreur lors de la recheche des candidature"))
        }
    })
});
router.get("/getfile", (req, res) => {
    var filename = req.query.filename;
    res.download("uploads/" + filename);
});

router.post("/valider", (req, res) => {
    const id_employee = req.body.id_employee;
    const id_candidature = req.body.id_candidature;

    if (isValidationUnique(id_candidature, id_employee)){
        validationModel.create({
            candidatureIdCandidature: id_candidature,
            employerIdEmployer: id_employee
        }).then(validation => {
            bchain.addBlock(JSON.stringify(validation));
            validerCandidature(id_candidature);
            res.send(returnSuccess("Vous avez Valider cette candidature"))
        });
    }
});
function isValidationUnique (id_candidature, id_employee) {
    return validationModel.count({ where: {
            candidatureIdCandidature: id_candidature,
            employerIdEmployer: id_employee
        } })
        .then(count => {
            if (count != 0) {
                return false;
            }
            return true;
        });
}
function validerCandidature(id_candidature){
    employeeModel.count().then(count => {
        validationModel.count({
            where: {
                candidatureIdCandidature : id_candidature
            }
        }).then(countCandidature => {
            if (countCandidature > count/2 ){
                CandidatureModel.findOne({
                    where:{
                        id_candidature : id_candidature
                    }
                }).then(candidature => {
                    candidature.valide = "valide";
                    candidature.save().then(
                        cand =>{
                            console.log("good");
                        }
                    )
                })
            }
        })
    })
};
router.post("/add", upload, (req, res) => {
    const candidatdata = req.body.candidat;
    const motivation = req.files[0].filename;
    const curriculum = req.files[1].filename;
    const id_offre = req.body.id_offre;
    const id_candidat = req.body.id_candidat;

    CandidatureModel.create({
        curiculum: curriculum,
        motivation: motivation,
        offreIdOffre: id_offre,
        candidatIdCandidat: id_candidat,
        valide:'en cour'
    }).then(result => {
        bchain.addBlock(JSON.stringify(result));
        res.send(returnSuccess("Votre candidature a ete ajoutee"))
    })
        .catch(err => res.send(returnError("erreur lors de l'enregistrement")));
});

module.exports = router;
