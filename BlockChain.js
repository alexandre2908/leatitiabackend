const transction = require("./Models/Transcations");
const crypto = require("crypto");
class Block {
    constructor(index, data, prevHash) {
        this.index = index;
        this.timestamp = Math.floor(Date.now() / 1000);
        this.data = data;
        this.prevHash = prevHash;
        this.hash=this.getHash();
    }
    getHash() {
        var encript=JSON.stringify(this.data) + this.prevHash + this.index + this.timestamp;
        var hash=crypto.createHmac('sha256', "secret")
            .update(encript)
            .digest('hex');
        // return sha(JSON.stringify(this.data) + this.prevHash + this.index + this.timestamp);
        return hash;
    }
}

module.exports = class BlockChain {
    constructor() {
        this.chain = [];
    }

    async addBlock(data) {
        let index = this.chain.length;
        let prevHash = this.chain.length !== 0 ? this.chain[this.chain.length - 1].hash : 0;
        let block = new Block(index, data, prevHash);
        await transction.create({
            index: block.index,
            timestamp: block.timestamp,
            data: block.data,
            prevHash: block.prevHash,
            hash: block.hash
        })
    }

    chainIsValid(){
        for(var i=0;i<this.chain.length;i++){
            if(this.chain[i].hash !== this.chain[i].getHash())
                return false;
            if(i > 0 && this.chain[i].prevHash !== this.chain[i-1].hash)
                return false;
        }
        return true;
    }
}
