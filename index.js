//module import

const express =  require("express");
const PORT = process.env.PORT || 5002;

const app = express();
const database = require("./Sequelize");


app.use(express.static('public'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Origin", "*");
    next();
});

//database
database.sync({force: false}).then(() => {
    console.log('Database & Tables created !');
});



//defining routes
const mainRoutes = require("./Routes/mainRoutes");
const candidatRoutes = require("./Routes/candidatRoutes");
const candidatureRoutes = require("./Routes/candidatureRoutes");
const offresRoutes = require("./Routes/offresRoutes");
const AdminRoutes = require("./Routes/AdminRoutes");
const EmployeesRoutes = require("./Routes/employeeRoutes");


//creating routes
app.use("/", mainRoutes);
app.use("/candidat", candidatRoutes);
app.use("/candidature", candidatureRoutes);
app.use("/offres", offresRoutes);
app.use("/admin", AdminRoutes);
app.use("/employee", EmployeesRoutes);


app.listen(PORT, () => console.log("listening on port " + PORT));
