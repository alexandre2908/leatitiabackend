const Sequelize = require("sequelize");
const databse = require("../Sequelize");
const candidatModel = require("../Models/CandidatModel");

const candidatureModel = databse.define('candidature', {
    id_candidature: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    curiculum: Sequelize.STRING,
    motivation: Sequelize.TEXT,
    valide: Sequelize.STRING
});

module.exports = candidatureModel;

