const Sequelize = require("sequelize");
const databse = require("../Sequelize");

const AdminModel = databse.define('transactions', {
    index: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    timestamp:Sequelize.STRING,
    data: Sequelize.TEXT,
    prevHash:Sequelize.STRING,
    hash:Sequelize.STRING
});

module.exports = AdminModel;
