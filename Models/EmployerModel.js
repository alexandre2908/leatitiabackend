const Sequelize = require("sequelize");
const database = require("../Sequelize");

const employerModel = database.define('employer', {
    id_employer: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    nom_complet: Sequelize.STRING,
    email: Sequelize.STRING,
    password:Sequelize.STRING,
    fonction: Sequelize.STRING
});


module.exports = employerModel;

