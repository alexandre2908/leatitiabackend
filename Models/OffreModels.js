const Sequelize = require("sequelize");
const databse = require("../Sequelize");
const candidature = require("./CandidatureModel");

const OffresModel = databse.define('offres', {
    id_offre: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    titre: Sequelize.STRING,
    date_fin: Sequelize.STRING,
    description :Sequelize.TEXT
});

OffresModel.hasMany(candidature);

module.exports = OffresModel;
