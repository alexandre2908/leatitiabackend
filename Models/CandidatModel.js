const Sequelize = require("sequelize");
const databse = require("../Sequelize");
const candidature = require("./CandidatureModel");

const candidatModel = databse.define('candidat', {
        id_candidat: {
            type:Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        name:Sequelize.STRING,
        email: Sequelize.STRING,
        numero_telephone: Sequelize.STRING,
        password:Sequelize.STRING
    });

module.exports = candidatModel;

