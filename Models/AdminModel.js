const Sequelize = require("sequelize");
const databse = require("../Sequelize");

const AdminModel = databse.define('admin', {
    id_admin: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    name:Sequelize.STRING,
    email: Sequelize.STRING,
    password:Sequelize.STRING
});

module.exports = AdminModel;

