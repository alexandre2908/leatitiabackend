const Sequelize = require("sequelize");
const databse = require("../Sequelize");

const validationModel = databse.define('validation', {
    id_validation: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    }
});

module.exports = validationModel;

