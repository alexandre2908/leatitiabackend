const Sequelize = require("sequelize");
const database = require("../Sequelize");
const employer = require("./EmployerModel");

const reseauxModel = database.define('reseaux', {
    id_reseaux: {
        type:Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    title: Sequelize.STRING
});

reseauxModel.hasMany(employer);

module.exports = reseauxModel;

